# vk-norm-bot (реализация c длинными запросами к API)

from vk_api.longpoll import VkLongPoll, VkEventType # длинные запросы к API
import vk_api                                       # vk_api
import random                                       # генерация рандомного числа

# ключ
key_vk = "KEY"
# подключение к VK
vk = vk_api.VkApi(token=key_vk)

# длинный запрос к API
longpoll = VkLongPoll(vk)

print("start")

# цикл по событиям длинного запроса с использованием слушателя
for event in longpoll.listen():
    
    # если событие является непрочитанным сообщением
    if event.type == VkEventType.MESSAGE_NEW:
        
        # id отправителя
        id = event.user_id
        
        # text полученного сообщения
        text = event.text
        
        # псевдослучайное число в диапазоне int64
        r = random.randint(0,2**64-1)
        
        # сообщение от пользователя (не от самого себя)
        if event.from_user and not (event.from_me):
            
            # если сообщение = "привет"
            if text.lower() == "привет":

                # ответ пользователю - "Привет!"
                vk.method("messages.send",{"peer_id":id,"message": "Привет!", "random_id":r})

            # иначе
            else:
                
                # ответ пользователю - "Не понимаю."
                vk.method("messages.send",{"peer_id":id,"message": "Не понимаю.", "random_id":r})
                
